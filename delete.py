# O C T O P U S
# Delete instance

import os
import sys
import shutil
import subprocess
   
# Execute vagrant commands   
subprocess.call(["vagrant", "destroy", "-f"])
shutil.rmtree(os.path.join(os.getcwd(), 'instances', str(sys.argv[1])))

print("Done Deal ;)")