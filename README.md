# O C T O P U S		
		
Wrappers to automate Vagrant workflows		
		
Commands:		
		
  master.py -h					-	Help		
  master.py -g					-	Get available BoxNames		
  master.py -c <boxName>		-	Create VM with BoxName specified		
  master.py -a					-	Get active instances		
  master.py -d <instanceName>	-	Delete specified instance			