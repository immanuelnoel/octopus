# O C T O P U S
# Create instance

import os
import sys
import time
import shutil
import subprocess

# System Vars
boxDirectory = "C:/Boxes/"

boxName = str(sys.argv[1]);

dirfmt = "%4d%02d%02d%02d%02d%02d"
root = os.getcwd()
instancename = dirfmt % time.localtime()[0:6] + "_" + boxName
instancepath = os.path.join(os.getcwd(), 'instances', instancename)

print('Attempting to bring up instance, ' + instancename + ' with box, ' + boxName)

# Setup instance directory
os.makedirs(instancepath)
shutil.copy(os.path.join(os.getcwd(), 'templates', 'Vagrantfile'), instancepath + '/Vagrantfile')
shutil.copy(os.path.join(os.getcwd(), 'templates', 'script.sh'), instancepath + '/script.sh')
shutil.copytree(os.path.join(os.getcwd(), 'templates', 'src'), instancepath + '/src')
os.chdir(instancepath)

# Update Vagrant file with the box name mentioned 
filedata = None
with open('Vagrantfile', 'r') as file :
    filedata = file.read()
filedata = filedata.replace('config.vm.box = ""', 'config.vm.box = "' + boxDirectory + boxName + '.box"')
with open('Vagrantfile', 'w') as file:
    file.write(filedata)
   
# Execute vagrant commands   
subprocess.call(["vagrant", "up"])
print(subprocess.check_output(["vagrant", "ssh-config"]))
subprocess.call(["vagrant", "ssh", "-c", "/opt/Octopus/getIP.sh"])

# Switch back to the orginal directory
os.chdir(root)

print("{'Instance' : '" + instancename + "'}")