# O C T O P U S
# Automating Vagrant Workflows

import os
import sys
import argparse
import ConfigParser

parser = argparse.ArgumentParser(description='OCTOPUS :: Vagrant Automated')
parser.add_argument('-g','--get', help='Get available BoxNames', required=False, action='store_true')
parser.add_argument('-c','--create', help='Create VM with BoxName specified', required=False)
parser.add_argument('-a','--active', help='Get active instances', required=False, action='store_true')
parser.add_argument('-d','--delete', help='Delete specified instance', required=False)
args = vars(parser.parse_args())


# Configuration
Config = ConfigParser.ConfigParser()
Config.read("config.ini")
BoxHome = Config.get("Settings", 'BoxHome', 0)

if(args['get'] == True):
    print("Available images,")
    for item in os.listdir(BoxHome):
        
        if(os.path.isfile(os.path.join(BoxHome, item))):
            print(item.replace('.box',''))

if(args['create']):    
    print("Attempting to create VM with requested image")
    
    # Attempt to bring up new box. BoxIdentifier sent as param
    os.system("create.py " + args['create'])

if(args['active'] == True):
    print("Active VMs,")
    
    for item in os.listdir(os.path.join(os.getcwd(), "instances")):
        if(os.path.isdir(os.path.join(os.getcwd(), "instances", item))):
            print(item)

if(args['delete']):
    print("Attempting to delete specified VM")
    
    # Attempt to bring down mentioned instance. Instance name sent as param
    os.system("delete.py " + args['delete'])


exit()